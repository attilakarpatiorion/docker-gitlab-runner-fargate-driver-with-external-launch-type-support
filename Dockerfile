FROM --platform=linux/amd64 gitlab/gitlab-runner:latest

# Install jq to be used for parsing the Gitlab API result
RUN apt-get update
RUN apt-get install -y jq

# Install gettext for envsubst
RUN apt-get install -y gettext-base

# Download dumb-init
ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64 /usr/local/bin/dumb-init
RUN chmod +x /usr/local/bin/dumb-init


#
# Docker Engine installation
#
RUN apt-get update
RUN apt-get install -y ca-certificates curl gnupg
    
RUN mkdir -m 0755 -p /etc/apt/keyrings
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
RUN echo "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian bullseye stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN apt-get update
RUN apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# -------------------------------------------------------------------------------------
# Execute a startup script.
# https://success.docker.com/article/use-a-script-to-initialize-stateful-container-data
# for reference.
# -------------------------------------------------------------------------------------
# Copy the config template files to be used for generating our runner and driver config
COPY config_runner_template.toml /tmp/
COPY config_driver_template.toml /tmp/
COPY docker-entrypoint.sh /usr/local/bin/
# Copy the fargate driver
COPY fargate /usr/local/bin/fargate-linux-amd64
RUN chmod +x /usr/local/bin/fargate-linux-amd64

RUN chmod +x /usr/local/bin/docker-entrypoint.sh \
    && ln -s /usr/local/bin/docker-entrypoint.sh / # backwards compat
ENTRYPOINT ["dumb-init", "docker-entrypoint.sh"]
